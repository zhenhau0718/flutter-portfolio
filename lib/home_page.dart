import 'package:camera/camera.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_portfolio1/camera_screen.dart';
import 'package:flutter_portfolio1/models/book.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.cameras}) : super(key: key);

  final List<CameraDescription> cameras;

  @override
  State<HomePage> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<HomePage> {
  final _firebaseAuth = FirebaseAuth.instance;

  Future<QuerySnapshot<Book>> getFirestoreData() async {
    final book = await FirebaseFirestore.instance
        .collection('books')
        .withConverter<Book>(
          fromFirestore: (snapshots, _) => Book.fromJson(snapshots.data()!),
          toFirestore: (book, _) => book.toJson(),
        )
        .get();

    return book;
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Home page"),
        actions: [
          IconButton(
              onPressed: () {
                _firebaseAuth.signOut();
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      body: FutureBuilder(
          future: getFirestoreData(),
          builder: (context, AsyncSnapshot<QuerySnapshot<Book>> snapshot) {
            if (snapshot.hasError) return Text('Error = ${snapshot.error}');
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }
            var bookList = snapshot.data?.docs;
            return ListView.builder(
                itemCount: bookList?.length,
                itemBuilder: (context, index) => Card(
                      child: Column(children: [
                        Text(bookList![index].data().name),
                        Text(bookList[index].data().isbn)
                      ]),
                    ));
          }),
      floatingActionButton: FloatingActionButton(onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CameraScreen(cameras: widget.cameras)),
        );
      }), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
