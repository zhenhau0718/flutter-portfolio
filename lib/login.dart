import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_portfolio1/home_page.dart';

enum FormStatus { register, login }

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _firebaseAuth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();

  FormStatus currentForm = FormStatus.login;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final secondPasswordController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void changeForm() {
    if (currentForm == FormStatus.login) {
      currentForm = FormStatus.register;
    } else {
      currentForm = FormStatus.login;
    }
    emailController.clear();
    passwordController.clear();
    secondPasswordController.clear();
    _formKey.currentState?.reset();
    setState(() {});
  }

  void showInvalidLoginMessage() {
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        backgroundColor: Colors.red,
        content: SizedBox(
          child: Text(
            "Wrong email/ password",
            style: TextStyle(color: Colors.white),
          ),
        )));
  }

  Future signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } on FirebaseAuthException catch (e) {
      showInvalidLoginMessage();
    }
  }

  Future registerUserWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    try {
      final userCredential = await _firebaseAuth
          .createUserWithEmailAndPassword(
            email: email,
            password: password,
          )
          .then((value) => changeForm());
    } on FirebaseAuthException catch (e) {}
  }

  String? get _errorText {
    if (passwordController.text != secondPasswordController.text) {
      return 'Passwords are not same';
    }

    // return null if the text is valid
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: currentForm == FormStatus.login
          ? AppBar(
              title: const Text("Login Page"),
            )
          : AppBar(
              title: const Text("Register Page"),
              leading: IconButton(
                icon: const Icon(Icons.close, color: Colors.white),
                onPressed: () => changeForm(),
              ),
            ),
      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: SizedBox(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/flutter-logo.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                validator: (input) {
                  if (input == null || input.isEmpty) {
                    return 'This field cannot be empty';
                  }
                  return null;
                },
                controller: emailController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Email'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: TextFormField(
                validator: (input) {
                  if (input == null || input.isEmpty) {
                    return 'This field cannot be empty';
                  }
                  return null;
                },
                controller: passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Password'),
              ),
            ),
            if (currentForm == FormStatus.register)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  validator: (input) {
                    if (input == null || input.isEmpty) {
                      return 'This field cannot be empty';
                    }
                    if (input != passwordController.text) {
                      return 'Passwords are not same';
                    }
                    return null;
                  },
                  controller: secondPasswordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter Password Again',
                    hintText: 'Enter Password Again',
                  ),
                ),
              ),
            const SizedBox(
              height: 50,
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: ElevatedButton(
                onPressed: () {
                  print(FirebaseAuth.instance.currentUser);
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }
                  if (currentForm == FormStatus.login) {
                    signInWithEmailAndPassword(
                        email: emailController.text,
                        password: passwordController.text);
                  } else {
                    registerUserWithEmailAndPassword(
                        email: emailController.text,
                        password: passwordController.text);
                  }
                },
                child: Text(
                  currentForm.name.toUpperCase(),
                  style: const TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            const SizedBox(
              height: 80,
            ),
            if (currentForm == FormStatus.login)
              RichText(
                text: TextSpan(children: [
                  const TextSpan(
                      text: "New User? ",
                      style: TextStyle(color: Colors.black)),
                  TextSpan(
                      text: "Create Account",
                      style: const TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => changeForm() as GestureTapCallback?)
                ]),
              ),
          ],
        ),
      ),
    );
  }
}
