import 'package:camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_portfolio1/home_page.dart';
import 'package:flutter_portfolio1/login.dart';

late List<CameraDescription> _cameras;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  _cameras = await availableCameras();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const StartingPage(),
    );
  }
}

class StartingPage extends StatefulWidget {
  const StartingPage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<StartingPage> createState() => _StartingPagePageState();
}

class _StartingPagePageState extends State<StartingPage> {
  final _auth = FirebaseAuth.instance;
  bool userLoggedIn = false;

  @override
  void initState() {
    verifyAuthState();
    super.initState();
  }

  verifyAuthState() async {
    _auth.authStateChanges().listen((user) {
      if (user == null) {
        setState(() {
          userLoggedIn = false;
        });
      } else {
        setState(() {
          userLoggedIn = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return userLoggedIn ? HomePage(cameras: _cameras) : const LoginPage();
  }
}
