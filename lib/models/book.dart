class Book {
  final String name;
  final String isbn;

  Book(this.name, this.isbn);

  Book.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        isbn = json['isbn'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'isbn': isbn,
      };
}
